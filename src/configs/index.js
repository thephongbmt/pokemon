import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from "redux-saga";

import history from './history';
import createReducer from '../reducer';
import createSaga from '../saga';

// init saga middleware
const sagaMiddleware = createSagaMiddleware();

// declare variable
const initReducer = {};
const injectReducer = {};
const middlewareConfigs = [
  sagaMiddleware
];

// create store
const store = createStore(createReducer(injectReducer), initReducer,  applyMiddleware(...middlewareConfigs));
store.runSaga = sagaMiddleware.run(createSaga);

// export config
export const storeConfig = store;
export const historyConfig = history;
export const API_POKEMON = 'https://pokeapi.co/api/v2'