import React, { memo } from 'react';
import './style.css'
const Hompage = () => {
  return <div className={'home_page'} >
    <h1>Wellcom to<img className={'home_page__logo'} height={60} width={80} src={require('../../assets/images/pokemon-logo.png')} /></h1>
    <img src={require('../../assets/images/wallpaper.png')} />
    </div>
}

export default memo(Hompage);