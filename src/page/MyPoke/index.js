import React,{ memo } from 'react';
import  MyPokeContainer  from '../../container/MyPoke'
const MyPokePage = () => {
  return <div className={'home_page'} >
    <MyPokeContainer />
    </div>
}

export default memo(MyPokePage);