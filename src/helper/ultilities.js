import { shuffle } from 'lodash'
export const getHeaderFromHeader = (res) => {

}

export const setHeaderFromHeader = () => {
  
}
export const buildEmptyObjArray = (length) => {
  let tmp = []
  for(let i = 0; i < length; i++){
    tmp.push({})
  }
  return tmp;
}
export const convertToDisplayNumber = (number, limitText, textReplace='') => {
  let tmp = number.toString();
  let missingZero = limitText - tmp.length;
  return textReplace.repeat(missingZero) + number;
}
export const convertToSelectData = ( data = [], nameKey ,valueKey) => {
  return data.map(e => ({
    name: e[nameKey],
    value: e[valueKey]
  }))
}
export const buildInfoPoke = (stats = []) =>  stats.map(st => {
    let name = st.stat.name;
    let value = st.base_stat;
    return { name, value }
  })


export const getPokemonAvatarById = (id, isHighQuality = true) => {
  if(isHighQuality){
    return `https://pokeres.bastionbot.org/images/pokemon/${id}.png`
  }
  return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`
}
export const getPokemonShinyAvatarById = (id) => {
  return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/${id}.png`
}

export const buildPercentByNumber = (arr = []) => {
  let tmpArr = [];
  arr.forEach((e) => {
    for(let i = 0; i < e; i++){
      tmpArr.push(e);
    }
  })
  return shuffle(tmpArr);
}

export const randomNumber = (min,max) => {
  return Math.floor(Math.random() * (max - min + 1) + min);
}
export const splitIdFromUrl = (url) => {
  let arrPath = url.split('/');
  return arrPath[arrPath.length - 2]
}