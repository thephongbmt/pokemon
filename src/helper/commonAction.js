
export const reduxAction = (type,data) => {
  return ({
    type,
    payload:data
  })
}

export const handleError = (error) => ({
  type: 'HANDLE_ERROR',
  payload: error
})