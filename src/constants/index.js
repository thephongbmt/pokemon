export const LIMIT_PER_PAGE = 30;
export const rarityType = {
  '50': {
    title: 'Common',
    color: '#999999',
  },
  '30': {
    title: 'UnCommon',
    color: '#009900',
  },
  '20': {
    title: 'Rare',
    color: '#1a75ff',
  },
  '10': {
    title: 'Epic',
    color: '#ff00ff',
  },
  '3': {
    title: 'Lengendary',
    color: '#ffa31a',
  },
  '1': {
    title: 'Universe',
    color: 'linear-gradient(to right,#ffca1a, #ffa31a, #8d1aff , #891aff)',
  }
 
} 
export const typePoke = {
  "normal": {
    color: "#cccccc",
    logo: ""
  },
  "fighting": {
    color: "#b30000",
    logo:""
  }, 
  "flying": {
    color: "#0077b3",
    logo:""
  }, 
  "poison": {
    color: " #b30086",
    logo:""
  }, 
  "ground":{
    color: "#b35900",
    logo:""
  }, 
  "rock": {
    color: "#8c8c8c",
    logo:""
  }, 
  "bug": {
    color: "#009900",
    logo:""
  }, 
  "ghost": {
    color: "#666699",
    logo:""
  }, 
  "steel": {
    color: "#8c8c8c",
    logo:""
  }, 
  "fire": {
    color: "#ff794d",
    logo:""
  }, 
  "water": {
    color: "#1ab2ff",
    logo:""
  }, 
  "grass": {
    color: "#006622",
    logo:""
  }, 
  "electric": {
    color: "#e6e62d",
    logo:""
  }, 
  "psychic": {
    color: "#ff99ff",
    logo:""
  }, 
  "ice": {
    color: "#b3e6ff",
    logo:""
  }, 
  "dragon": {
    color: "#e60000",
    logo:""
  }, 
  "dark": {
    color: "#262626",
    logo:""
  }, 
  "fairy": {
    color: "#ffb3ff",
    logo:""
  }, 
  "unknown": {
    color: "#b30000",
    logo:""
  }, 
  "shadow": {
    color: "#669999",
    logo:""
  }, 
}