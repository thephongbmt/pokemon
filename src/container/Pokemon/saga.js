import { put, takeLatest } from 'redux-saga/effects';
import requestAPI from '../../helper/requestApi';
import { showLoading } from '../Loading/action';
import { reduxAction, handleError } from '../../helper/commonAction';

function *getListPokemon ({type,payload}) {
  try {
    yield put(showLoading(true));
    let { data } = yield requestAPI.get('/pokemon',payload.query);
    yield put(reduxAction("GET_LIST_POKEMON_SUCCESS", { 
      pokemons: data.results,
      total: data.count,
      offset: payload.query.offset || 0 
    }));
  } catch(err){
    yield put(handleError(err));
  } finally{
    yield put(showLoading(false));
  }
}

export function* watchPokemon() {
  yield takeLatest('GET_LIST_POKEMON', getListPokemon);
}