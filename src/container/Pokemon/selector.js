import { createSelector } from 'reselect';

export const makeSelectorPokemon =  createSelector(
  state => state.pokemonReducer.pokemons,
  pokemons => pokemons
);
export const makeSelectorOffset =  createSelector(
  state => state.pokemonReducer.offset,
  offset => offset
);
export const makeSelectorTotal =  createSelector(
  state => state.pokemonReducer.total,
  total => total
);