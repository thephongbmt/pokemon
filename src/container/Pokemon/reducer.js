import produce from 'immer';
const initReducer = {
  offset:0,
  total:0, 
  pokemons: []
}

const pokemonReducer = (state = initReducer, { type, payload }) => produce(state, (draft) => { 

  switch(type){
    case 'GET_LIST_POKEMON_SUCCESS': {
      draft.pokemons = draft.pokemons.concat(payload.pokemons);
      draft.total = payload.total;
      draft.offset = payload.offset;
      break;
    }
    default: return state;
  }
})

export default pokemonReducer;