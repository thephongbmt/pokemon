import React, { memo, useEffect, useRef, useCallback } from 'react';
import get from 'lodash.get';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import Grid from '@material-ui/core/Grid';
import Card from '../../component/Card';

import { convertToDisplayNumber, splitIdFromUrl, getPokemonAvatarById } from '../../helper/ultilities'

import { LIMIT_PER_PAGE } from '../../constants';
import { getListPokemon } from './action';
import {makeSelectorPokemon,makeSelectorOffset, makeSelectorTotal} from './selector'
import './style.css'

const PokemonDex = () => {
 const mainRef = useRef(null);
 const dispatch = useDispatch();
 const history = useHistory();
 
 let pokemons = useSelector(makeSelectorPokemon);
 let offset = useSelector(makeSelectorOffset);
 let total = useSelector(makeSelectorTotal);

 useEffect(() => {
    if(!pokemons || pokemons.length === 0 ){
      dispatch(getListPokemon({ limit: LIMIT_PER_PAGE }));
    }
  },[]);

  const getDetail = useCallback((id) => {
    history.push('/pokemon/'+id);
  }, []);

  const renderRows = () => {
    return pokemons.map(poke => {
      let indexPoke = splitIdFromUrl(get(poke,'url',''))
      let numberDisplay = convertToDisplayNumber(indexPoke, 4 ,'0');
      let name = get(poke,'name','');
      return (
      <Grid key={indexPoke} item >
        <Card 
          id={indexPoke}
          onClick={getDetail}
          description = {`No #${numberDisplay}`}
          title = {name.toUpperCase()}
          content={''}
          alt={name}
          cover = {getPokemonAvatarById(indexPoke,false)}
        />
      </Grid>
    )})
  }
 const handleScroll = () => {
  let { clientHeight, scrollTop,  scrollHeight } = mainRef.current;
  if(clientHeight + scrollTop === scrollHeight && total >= offset){
    dispatch(getListPokemon({ limit: LIMIT_PER_PAGE, offset: offset + LIMIT_PER_PAGE }));
  }
 }
 return (
   <div className={'pokemon__dex__container'} ref={mainRef} onScroll={handleScroll} >
      <Grid container spacing={2}>
        {renderRows()}
      </Grid>
    </div>
  )
}

export default memo(PokemonDex);