import React, { useCallback } from 'react';
import Skeleton from 'react-loading-skeleton';
import { useHistory } from 'react-router-dom';
import { getPokemonAvatarById } from '../../helper/ultilities';
const Evolution =  ({ evolutions = [] }) => {
  const history = useHistory();
  const handleClick = useCallback((id) => {
    history.push(`/pokemon/${id}`);
  }, []);

  const renderRow = () => (evolutions.map((arr,index) => (
      <tr key={index}>
        <td>
          {arr.map((e) => <img style={{cursor:'pointer'}} onClick={() => handleClick(e.id)}  key={e.name} src={getPokemonAvatarById(e.id,false)} />)}
        </td>
      </tr>
    )))
  return (
    <>
    { evolutions.length !== 0 ? <table><tbody>{renderRow()}</tbody></table> : <Skeleton height={100} duration={2}/> }
    </>
  )
}

export default Evolution;