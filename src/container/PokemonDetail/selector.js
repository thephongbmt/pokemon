import { createSelector } from 'reselect';
export const makeSelectorPokemon =  createSelector(
  state => state.pokemonDetailReducer.pokemon,
  total => total
);