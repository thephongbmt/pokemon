import React, { memo, useEffect, lazy , useCallback, Suspense} from 'react';
import { upperFirst, get } from 'lodash';
import { useDispatch, useSelector} from 'react-redux';
import { useParams, useHistory } from 'react-router-dom';
import useEvolution from '../../hook/evolution';

import { Icon } from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import Grid from '@material-ui/core/Grid';
import Chip from '@material-ui/core/Chip';
import IconButton from '@material-ui/core/IconButton';

import Card from '../../component/Card';
import InfoPoke from '../../component/InfoPokemon';

import {  getPokemonAvatarById, getPokemonShinyAvatarById, buildInfoPoke, convertToDisplayNumber } from '../../helper/ultilities';
import { typePoke } from '../../constants';
import { getDetail } from './action';
import { makeSelectorPokemon } from './selector';

import './style.css';

const Evolution = lazy(()=> import('../../container/Evolution'));

const PokemonDetail = (props) => {
  const dispatch = useDispatch();
  const history  = useHistory();
  const { id } = useParams();
  const evolutions = useEvolution(id);
  let pokemonDetail = useSelector(makeSelectorPokemon);

  useEffect(() => {
    dispatch(getDetail(id));
  }, [id]);
  
  const renderAbility = (arr) => {
    return arr.map(e => (
      <Chip
        key={e}
        className={'card__pokemon__type'}
        style={{ backgroundColor:'rgb(182, 188, 226)', color:'white',padding: 10 }}
        label={e.toUpperCase()}
      />
    ))
  }
  const renderType = () => {
    if(!pokemonDetail.types){
      return <></>;
    }
    return pokemonDetail.types && pokemonDetail.types.map(ele => {
      let type = ele.type.name;
      return (<Chip
        key={type}
        className={'card__pokemon__type'}
        icon={
        <Icon  style={{ color:'white'}} >
         <img src={require(`../../assets/logo/pokemon-type/${type}.svg`)} height={25} width={25}/>
        </Icon>
        }
        style={{ background: typePoke[type].color, color:'white',padding: 10 }}
        label={type.toUpperCase()}
      />)
    })
  }
  const handleNavigatePreNext = useCallback((type) => {
    if(type === 'next'){
      history.push(`/pokemon/${+(id) + 1}/`)
    }else{
      history.push(`/pokemon/${+(id) - 1}/`)
    }
  },[])

  pokemonDetail = id != pokemonDetail.id ? {} : pokemonDetail;
  let infos = buildInfoPoke(pokemonDetail.stats);
  let abilities = get(pokemonDetail,'abilities',[]).map( e => e.ability.name);
  let numberDisplay = convertToDisplayNumber(id, 4 ,'0')

  return (
    <div className={'cotainer__detail_pokemon'}>
      <div className='btn_group__pokemon_detail'>
        <div>{id == '1' ? '' : <IconButton onClick={() => handleNavigatePreNext('pre')}><NavigateBeforeIcon />Prev</IconButton>}</div>
        <div><IconButton onClick={() => handleNavigatePreNext('next')}>Next<NavigateNextIcon /></IconButton></div>
      </div>
      <Grid container spacing={3}>
        <Grid item lg={3} md={5} xs={12}>
        <Card 
            className={'card__pokemon__detail'}
            id={id}
            onClick={getDetail}
            description = {`No #${numberDisplay}`}
            title = {upperFirst(pokemonDetail.name)}
            content={ renderType() }
            alt={pokemonDetail.name}
            cover = {id && getPokemonAvatarById(id)}
          />
        </Grid>
        <Grid item xs={9} md={7} xs={12}>
          <h2>Information</h2>
          <ul>
            <li>Height: {pokemonDetail.height} Feets</li>
            <li>Weight: {pokemonDetail.weight} Pounds</li>
          </ul>
          <InfoPoke infos={infos} maxPoint={278} />
        </Grid>
        <Grid item md={4}>
          <h2>Evolution</h2>
          <Suspense fallback={`Loading....`} >
            <Evolution evolutions={evolutions} />
          </Suspense>
        </Grid>
        <Grid item md={4}>
          <h2>Shiny</h2>
          <img src={getPokemonShinyAvatarById(id)}/>
        </Grid>
        <Grid item md={4}>
          <h2>Abilities</h2>
            {renderAbility(abilities)}
        </Grid>
      </Grid>
    </div>
  )
}

export default memo(PokemonDetail);