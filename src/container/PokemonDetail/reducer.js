import produce from 'immer';
const initReducer = {
  pokemon: {}
}

const pokemonDetailReducer = (state = initReducer, { type, payload }) => produce(state, (draft) => { 
  switch(type){
    case 'GET_POKEMON_DETAIL_SUCCESS': {
      draft.pokemon = payload.pokemon
      break;
    }
    default: return state;
  }
})

export default pokemonDetailReducer;