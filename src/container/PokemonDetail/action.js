export const getDetail = (id) => ({
  type: 'GET_POKEMON_DETAIL',
  payload: { id }
})