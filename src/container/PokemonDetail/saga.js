import { put, takeLatest } from 'redux-saga/effects';
import requestAPI from '../../helper/requestApi';
// import { showLoading } from '../Loading/action';
import { reduxAction, handleError } from '../../helper/commonAction';

function *getDetailPokemon ({ type, payload }) {
  try {
    // yield put(showLoading(true));
    let { data } = yield requestAPI.get('/pokemon/' + payload.id + '/');
    yield put(reduxAction("GET_POKEMON_DETAIL_SUCCESS", { pokemon : data }));
  } catch(err){
    yield put(handleError(err));
  } finally{
    // yield put(showLoading(false));
  }
}

export function* watchPokemonDetail() {
  yield takeLatest('GET_POKEMON_DETAIL', getDetailPokemon);
}