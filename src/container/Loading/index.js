import React, { memo } from 'react';
import Loading from '../../component/Loading'
import { useSelector } from 'react-redux';

const LoadingContainer = () => {
const isLoading = useSelector(state => state.loadingReducer.isLoading);
 return (<Loading isLoading={isLoading} />)
}

export default memo(LoadingContainer);