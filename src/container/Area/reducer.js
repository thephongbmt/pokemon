import produce from 'immer';
const initReducer = {
  areas:[],
}

const areaReducer = (state = initReducer, { type, payload }) => produce(state, (draft) => { 

  switch(type){
    case 'GET_LIST_AREA_SUCCESS': {
      draft.areas = payload.areas;
      break;
    }
    default: return state;
  }
})

export default areaReducer;