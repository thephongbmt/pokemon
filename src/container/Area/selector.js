import { createSelector } from 'reselect';
import { splitIdFromUrl } from '../../helper/ultilities'
export const makeSelectorArea =  createSelector(
  state => state.areaReducer.areas,
  areas => areas.map(e => {
    let id = splitIdFromUrl(e.url);
    return { ...e, id}
  })
);