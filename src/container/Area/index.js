import React, { useEffect, useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom'
import Area from '../../component/Area';
import { getArea } from './action';
import { makeSelectorArea } from './selector'
import './style.css'

const AreaContainer = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const areas = useSelector(makeSelectorArea);
  useEffect(() => {
      dispatch(getArea());
  },[])


  const hanldOnClick =  useCallback((id)=> {
    history.push('/area/'+id);
  },[])

  const renderArea = () => {
    return areas.map(e => {
      return <Area key={e.id} title={e.name} onClick={ hanldOnClick } id={e.id} cover={require(`../../assets/area/${e.name}.jpg`)}/>
    })
  }
  return (<div className={'container__area'}>
    {renderArea()}
  </div>)
}

export default AreaContainer;