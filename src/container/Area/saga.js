import { put, takeLatest } from 'redux-saga/effects';
import requestAPI from '../../helper/requestApi';
import { showLoading } from '../Loading/action';
import { reduxAction, handleError } from '../../helper/commonAction';

function *getListArea ({ type, payload }) {
  try {
    yield put(showLoading(true));
    let { data } = yield requestAPI.get('/pal-park-area');
    yield put(reduxAction("GET_LIST_AREA_SUCCESS", { 
      areas: data.results,
    }));
  } catch(err){
    yield put(handleError(err));
  } finally{
    yield put(showLoading(false));
  }
}

export function* watchArea() {
  yield takeLatest('GET_LIST_AREA', getListArea)
}