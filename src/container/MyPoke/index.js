import React, { memo, useState, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { upperFirst, get } from 'lodash';

import Grid from '@material-ui/core/Grid';
import Chip from '@material-ui/core/Chip';
import Card from '../../component/Card';

import { convertToDisplayNumber, splitIdFromUrl, getPokemonAvatarById } from '../../helper/ultilities'
import { setLocalStorage, getLocalStorage } from '../../helper/localStorage';

import './style.css'
const MyPoke = () => {
  const [pokemons,setPokemon] = useState([]);
  const history = useHistory();
  const pokemonStore = getLocalStorage('mypoke');
  if(pokemons.length === 0 && pokemonStore){
    setPokemon(JSON.parse(pokemonStore));
  }
  const getDetail = useCallback((id) => {
    history.push('/pokemon/'+id);
  },[])
  const handleRelease = (e, id) => {
    e.stopPropagation();
    let indexRemove = pokemons.findIndex(e => e.id === id);
    pokemons.splice(indexRemove, 1);
    setLocalStorage('mypoke',JSON.stringify(pokemons));
    setPokemon([...pokemons]);
  }
  const renderRows = () => {
    return pokemons.map((pokeDetail) => {
      let poke = pokeDetail.pokemon_species;
      let id = splitIdFromUrl(get(poke,'url',''))
      let numberDisplay = convertToDisplayNumber(id, 4 ,'0');
      let name = get(poke,'name','');
      return (
        <div className='mypokemon' key={id}>
        <Grid item >
        <Card 
            // className={'card-poke-detail'}
            id={id}
            onClick={getDetail}
            description={`No #${numberDisplay}`}
            title = {upperFirst(name)}
            content={<Chip
              className={'pokemon-type'}
              style={{ backgroundColor:pokeDetail.color, backgroundImage: pokeDetail.color, color:'white',padding: 10 }}
              label={ pokeDetail.type } /> }
            alt={name}
            cover = {id && getPokemonAvatarById(id)}
          />
        </Grid>
        <div style={{marginTop:10}}>
            
          <button className='mypokemon__cotainer__header__btn' onClick={(e) => handleRelease(e, pokeDetail.id)} variant="contained" color="secondary">Release</button>
        </div>
      </div>
    )})
  }
  return <div className={'pokemon-dex-container'} >
      { pokemons.length === 0 ?
       'Go Area to catch new Pokemon' : 
      <Grid container  spacing={2}>{renderRows()}</Grid> }
    </div>
}

export default memo(MyPoke);