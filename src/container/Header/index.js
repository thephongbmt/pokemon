import React, { memo } from 'react';
import Button from '@material-ui/core/Button';
import Image from '../../component/Image';
import NavBar from '../../component/NavBar';
import './style.scss';
import { Link } from 'react-router-dom';

const Header = ({ menus = [] }) => {
  const Menus = () => {
    return menus.map(menu => <Link key={menu.path} className={'navbar__menu'} to={menu.path}><Button key={menu.name} color="inherit">{menu.name}</Button></Link> )
  }
  return (
      <NavBar className={'navbar'}>
        <Image src={require('../../assets/images/pokemon-logo.png')} alt={'logo'} height={100} width={100}/>
        <div>
          <Menus />
        </div>
      </NavBar>
  )
}

export default memo(Header);