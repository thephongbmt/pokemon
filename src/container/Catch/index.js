import React, { useState, useCallback } from 'react';
import { useParams } from 'react-router-dom';
import { uniq, upperFirst } from 'lodash';
import get from 'lodash.get';
import { createSelector } from 'reselect';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Card from '../../component/Card';

import usePokemonArea from '../../hook/pokemonArea';

import { rarityType } from '../../constants';
import { setLocalStorage, getLocalStorage } from '../../helper/localStorage';
import * as ultilities from '../../helper/ultilities';
import './style.css'

const Catch = () => {
  const { id } = useParams();
  const pokemonArea = usePokemonArea(id);
  const [isOpenDialog, setOpenDialog] = useState(false);
  const [pokemonRandom, setPokemonRandom] = useState({});
  const handleCloseDialog = () => {
    setOpenDialog(false);
  }
  const handleCatchPokemon = useCallback((poke) => {
    let myPoke = getLocalStorage('mypoke');
    if(myPoke){
      myPoke = JSON.parse(myPoke);
      myPoke.push(poke);
    } else{
      myPoke = [poke];
    }
    setLocalStorage('mypoke', JSON.stringify(myPoke));  
    setOpenDialog(false);
  },[]);
  const handleFindPokemon =() => {
    let randomArr = uniq(pokemonArea.map(e => e.rate));
    randomArr = ultilities.buildPercentByNumber(randomArr);
    let randomNumber = ultilities.randomNumber(0, randomArr.length - 1);
    let listWithType = pokemonArea.filter(e => e.rate === randomArr[randomNumber]);
    let randomNumberPoke = ultilities.randomNumber(0, listWithType.length - 1);
    let pokemon = listWithType[randomNumberPoke];
    let type = rarityType[pokemon.rate];
    pokemon.id = ultilities.splitIdFromUrl(get(pokemon,'pokemon_species.url',''));
    pokemon.type = type.title;
    pokemon.color = type.color;
    setPokemonRandom(pokemon);
    setOpenDialog(true);
  }
  return (
    <div className={'catching__pokemon'}>
      <h1>Click Pokemon Ball to Catch Pokemon</h1>
      <img onClick={handleFindPokemon} className={'catching__pokemon__ball'}  src={require('../../assets/images/pokeball.png')}/>
      <Dialog
        open={isOpenDialog}
        onClose={handleCloseDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          You found A Pokemon {' '} 
          <Chip
            className={'pokemon-type'}
            style={{ backgroundColor:pokemonRandom.color, backgroundImage: pokemonRandom.color, color:'white',padding: 10 }}
            label={pokemonRandom.type} />
        </DialogTitle>
        <DialogContent>
              <Card 
                id={pokemonRandom.id}
                description = {pokemonRandom.id && `No #${ultilities.convertToDisplayNumber(pokemonRandom.id, 4 ,'0')}`}
                title={pokemonRandom.id && upperFirst(get(pokemonRandom,'pokemon_species.name',''))}
                content={<Chip
                  className={'pokemon-type'}
                  style={{ backgroundColor:pokemonRandom.color, backgroundImage: pokemonRandom.color, color:'white',padding: 10 }}
                  label={pokemonRandom.type} />}
                cover = {pokemonRandom.id && ultilities.getPokemonAvatarById(pokemonRandom.id)}
              />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDialog} color="primary">
            Release
          </Button>
          <Button onClick={() => handleCatchPokemon(pokemonRandom)} color="primary" autoFocus>
            Catch
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
  
}

export default Catch;