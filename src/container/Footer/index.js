import React, { memo } from 'react';
import './style.css';
const Footer = (props) => {
  return (
     <div className={'footer'}>
       <div>
         <img className={'footer__pika'} src={require('../../assets/images/pikachu-hello.png')} />
       </div>
     </div>
  )
}
export default memo(Footer);