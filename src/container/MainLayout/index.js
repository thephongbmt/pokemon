import React, { memo } from 'react';
import Header from '../Header';
import Footer from '../Footer';
import './style.css'
const MainLayout = (props) => {
  return (
  <>
    <Header menus={props.menus}></Header>
    <div className={'main__layout'}>
      <div className={'main__layout__card'}>
        {props.children}
      </div>
    </div>
    <Footer></Footer>
  </>
  )
}

export default memo(MainLayout);