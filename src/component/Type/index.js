import React, { memo } from 'react';
import Chip from '@material-ui/core/Chip';

const Type = ({ title, icon, color }) => {
  return <Chip
      key={type}
      className={'pokemon_type pokemon_type__text'}
      icon={icon}
      style={{ background: color }}
      label={title}
    />
}
export default memo(Type);
