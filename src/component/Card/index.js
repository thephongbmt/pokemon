import React, { memo } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import Skeleton from 'react-loading-skeleton';

import './style.css'
const CustomCard = ({ id ,title, description, type, content , avatar, cover, alt, className, onClick }) => {
  const handleOnClick = () => {
    return onClick && onClick(id);
  }
  console.log('render card', title)
  return (
    <div className={`card ${className}`} onClick={handleOnClick} >
    <div className='card__header'>
      <div>
        { title ?  (avatar ? avatar : <Avatar src={require('../../assets/images/pokeball.png')} />) : <Skeleton height={50} width={50} circle={true} duration={2} />}
      </div>
      <div  className='card__header__title'>
        <h5>{ title ? title.toUpperCase() : <Skeleton height={20} width={100} duration={2} />}</h5>
        <span>{ title ? description : <Skeleton height={20} width={100} duration={2} /> }</span>
      </div>
      <div>
        {type}
      </div>
    </div>
    <div className='card__header__img'>
     { title ? <img alt={alt} src={cover} width={230} /> : <Skeleton height={200} width={300} duration={2} /> }
    </div>
    <div>
    { title ? content : <Skeleton height={30} width={200} duration={2} /> }
    </div>
  </div>
  )
}
export default memo(CustomCard);