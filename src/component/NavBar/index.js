import React, { memo } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import './style.css'
const NavBar = (props) => {
return <AppBar  position="static">
    <Toolbar className={'toolbar'}>
      {props.children}
    </Toolbar>
</AppBar>
  
}

export default memo(NavBar);