import React from 'react';
import { upperFirst } from 'lodash';
import LinearProgress from '@material-ui/core/LinearProgress';
import Skeleton from 'react-loading-skeleton';

import './style.css'
const InfoPoke = ({maxPoint ,title, infos = []}) => {
  const Info = ({info}) => {
    return (
      <div >
        <br />
        {`${upperFirst(info.name)} ${info.value}/${maxPoint}`}
        <LinearProgress className={'progress'}  variant="determinate"  value={(info.value/maxPoint)*100} />
      </div>
    )
  }
  return (
    <div>
        <h2>{title}</h2>
        <hr />
        <div>
           {
              infos.length !== 0 ? infos.map(info => <Info key={info.name} info={info} /> ) :
              <Skeleton height={300}  duration={2} />
           }
        </div>
      </div>
  ) 
}

export default InfoPoke;