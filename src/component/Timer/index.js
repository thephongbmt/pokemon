import React, { memo,useEffect, useState } from 'react';

const Counter = () => {
  let [count,setCount] = useState(0);
  useEffect(() => {
   let timer = setTimeout(() => {
      setCount(count+1);
    },1000);
    return () => clearTimeout(timer);
  })
  console.log('Timer: re-render',count)
  return (<div>
    Count: {count}
  </div>)
}

export default memo(Counter);