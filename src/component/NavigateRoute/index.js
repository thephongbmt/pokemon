import React from 'react';
import { Route, Redirect } from 'react-router-dom';

let NavigateRoute = (isRedirect, redirectPath) => ({ component:Component,...propsRoute }) => {
  return (
      <Route {...propsRoute}>
         {
           isRedirect ?  <Component /> : <Redirect to={{ pathname:redirectPath , state: { from: propsRoute.location } }} />
         }
      </Route>
    )
}
export default NavigateRoute;
