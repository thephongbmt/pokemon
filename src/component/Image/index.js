import React, { memo } from 'react';
const Image = (props) => {
  return  <img {...props} />
}

export default memo(Image);