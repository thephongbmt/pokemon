import React, { memo } from 'react';
import './style.css'
const Loading = ({isLoading}) => {
 return (<div className='loading' hidden={!isLoading} />)
}

export default memo(Loading);