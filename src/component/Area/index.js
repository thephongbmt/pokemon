import React, { memo } from 'react';
import './style.css'

const Area = ({ title, className, onClick,id , cover}) => {
  const hanldOnClick = () => {
    return onClick(id);
  }
  console.log('Area: render');
  return (
  <div onClick={hanldOnClick} className={`container__area__detail ${className}`} style={{ backgroundImage:`url(${cover})` }}>
    <h3>{title}</h3>
  </div>
  )
}

export default memo(Area);