import React from 'react';
import MainLayout from './container/MainLayout';
import Loading from './container/Loading';
import  InitRoute  from "./route";
import './App.css';

function App() {
  return (
    <>
      <Loading isLoading={false} />
      <InitRoute />
    </>
  );
}

export default App;
