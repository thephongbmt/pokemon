import { combineReducers } from 'redux';
import loadingReducer from '../container/Loading/reducer';
import pokemonReducer from '../container/Pokemon/reducer';
import pokemonDetailReducer from '../container/PokemonDetail/reducer';
import areaReducer from '../container/Area/reducer';
export default function configureStore(injectStore) {
  return combineReducers({
    ...injectStore,
    loadingReducer,
    pokemonReducer,
    pokemonDetailReducer, 
    areaReducer
});
}