import React from 'react';
import {
  Switch
} from 'react-router-dom'
import PublicRoute from '../component/NavigateRoute/PublicRoute';
import PrivateRoute from '../component/NavigateRoute/PrivateRoute';
import MainLayout from '../container/MainLayout';
import { initRoute } from './app';



const filterRouteType = (routes) => {
  return {
    publicRoutes: routes.filter(route => !route.isAuthenRoute),
    privateRoutes: routes.filter(route => route.isAuthenRoute),
    menuSideBars: routes.filter(route =>  route.isShowSideBar),
  }
}

const InitRoute = () => {
  let { publicRoutes, privateRoutes, menuSideBars } = filterRouteType(initRoute);
  return (
          <Switch>
          <MainLayout menus = {menuSideBars} >
             <React.Suspense fallback={"..."}>
                { privateRoutes.map(route => <PrivateRoute key={route.path} {...route} />) }
                { publicRoutes.map(route => <PublicRoute key={route.path}  {...route} />) }
              </React.Suspense>
            </MainLayout>
        </Switch>

  )
}

export default InitRoute;