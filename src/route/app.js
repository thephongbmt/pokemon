
import { lazy } from 'react';
export const HomePageLoadable = lazy(()=> import('../page/HomePage'));
export const RegisterLoadable = lazy(()=> import('../page/Register'));
export const LoginLoadable = lazy(()=> import('../page/Login'));
export const PokemonLoadable = lazy(()=> import('../page/Pokemon'));
export const PokemonDetailLoadable = lazy(()=> import('../page/PokemonDetail'));
export const AreaLoadable = lazy(()=> import('../page/Area'));
export const CatchLoadable = lazy(()=> import('../page/Catch'));
export const MyPokeLoable = lazy(()=> import('../page/MyPoke'));

export const initRoute = [
  // Public
  { path: '/', isAuthenRoute: false , component: HomePageLoadable, name:'HomePage', isShowSideBar:true, exact: true},
  { path: '/login', isAuthenRoute:false, component: LoginLoadable, name:'Login', isShowSideBar:false, exact: true },
  { path: '/register', isAuthenRoute: false , component: RegisterLoadable, name:'Register', isShowSideBar:false, exact: true},
  { path: '/my-pokemon', isAuthenRoute: false , component: MyPokeLoable, isShowSideBar:true, name:`My_Pokemon`, exact: true},
  { path: '/pokemon-dex', isAuthenRoute: false , component: PokemonLoadable, name:`Pokemon_Dex`, isShowSideBar:true, exact: true},
  { path: '/pokemon/:id', isAuthenRoute: false , component: PokemonDetailLoadable, exact: true},
  //private  
  { path: '/area', isAuthenRoute: true , component: AreaLoadable, name:'Area', isShowSideBar:true, exact: true},
  { path: '/area/:id', isAuthenRoute: true , component: CatchLoadable, name:'AreaDetail', isShowSideBar:false, exact: true},
];
