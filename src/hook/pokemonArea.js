import { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import get from 'lodash.get'
import requestApi from '../helper/requestApi';
import { showLoading } from '../container/Loading/action';
const usePokemonArea = (idArea) => {
  const dispatch = useDispatch();
  const [pokemonArea, setPokemonAreas] = useState([]);
  useEffect(() => {
    dispatch(showLoading(true))
    requestApi.get(`/pal-park-area/${idArea}`).then(res => {
      let data = get(res,'data.pokemon_encounters',[]);
      setPokemonAreas(data);
      dispatch(showLoading(false))
    })
  },[idArea]) 
  return pokemonArea;
}
export default usePokemonArea