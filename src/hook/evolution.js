import { useEffect, useState } from 'react';
import get from 'lodash.get';
import requestAPI from '../helper/requestApi';
import {  splitIdFromUrl } from '../helper/ultilities'

const useEvolution = (id) => {
  let [evolution, setEvolution] = useState([]);
  useEffect(() => {
    requestAPI.get('/pokemon-species/'+id).then((resSpecies) => {
      let urlEvolution = get(resSpecies,'data.evolution_chain.url','');
      let arrPath = urlEvolution.split('/').filter(e => e);
      if(arrPath.length !== 0){
        let idEvolution = arrPath[arrPath.length - 1];
        requestAPI.get('/evolution-chain/' + idEvolution).then(resEvolution => {
          let evolutionDetail = get(resEvolution, 'data.chain','');
          setEvolution(getEvolution([evolutionDetail], 'evolves_to'));
        });
      }
    })
   
  }, [id]);
  return evolution;
};

const getEvolution = (eve, fieldName) => {
  let tmp = [];
  const recusiver =  (arr, fieldName ,memory, prevId) => {
    for(let e of arr){
      let currentId = splitIdFromUrl(get(e,'species.url'))
      memory.push({
        name: e.species.name,
        id: currentId
      });
      if(e[fieldName] && e[fieldName].length !== 0){
        return recusiver(e[fieldName],fieldName, memory, currentId)
      }
      // end of children reset
      tmp.push(memory);
      memory = [];
    }
  }
  recusiver(eve, fieldName, []);
  return tmp;
}

export default useEvolution;
