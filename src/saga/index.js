import { all } from 'redux-saga/effects'
import { watchPokemon } from '../container/Pokemon/saga';
import { watchPokemonDetail } from '../container/PokemonDetail/saga';
import { watchArea } from '../container/Area/saga';


export default function* rootSaga() {
  yield all([
    watchPokemon(),
    watchPokemonDetail(),
    watchArea()
  ])
}